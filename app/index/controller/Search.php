<?php

namespace app\index\controller;
use app\index\service\BaseService;

class Search extends Base
{
	
	//搜索
	public function index(){
		
		$keyword = $this->request->param('keyword','','strval');
		if(empty($keyword)) $this->error('关键词不能为空');
		
		$this->view->assign('where','title like "%'.$keyword.'%" and status = 1');
		$this->view->assign('media', baseService::getMedia());  //网站关键词描述信息
		$this->view->assign('pid',0);
		$this->view->assign('class_name','搜索结果');
		$this->view->assign('position','当前位置:搜索结果');
		$default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
		return $this->display($default_themes.'/search');
	}
	
	
	
}
